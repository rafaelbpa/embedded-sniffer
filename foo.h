#ifndef FOO_H_
#define FOO_H_

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

int isLogAlreadyCreated = 0;
char logName[80];

char *getLogName() {

	if(isLogAlreadyCreated == 0) {

		time_t rawtime;
		struct tm * timeinfo;
		char buffer[80];
		time (&rawtime);
		timeinfo = localtime (&rawtime);
		strftime (buffer,16,"%G%m%d%H%M%S",timeinfo);
		strcpy(logName, buffer);
		strcat(logName, ".txt");
		isLogAlreadyCreated = 1;
		return logName;
	} else {
		return logName;
	}
}

void logNetwork(int hasInternet)
{
	
	FILE *f = fopen(getLogName(), "a");
	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}
	
	fprintf(f, "%d", hasInternet);
	
	fclose(f);
}

int checkNetwork()
{
	char *site = "ifce.edu.br";
	char *porta = "80";

        struct addrinfo hints, *res, *p;
        int status, sockfd;
        char ipstr[INET6_ADDRSTRLEN];
 
        //if (argc != 3) {
          //  fprintf(stderr,"usage: connectip hostname port\n");
            //return 1;
        //}
 
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_UNSPEC; // AF_INET or AF_INET6 to force version
        hints.ai_socktype = SOCK_STREAM;

        if ((status = getaddrinfo(site, porta, &hints, &res)) != 0) {
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status)); // sem rede e sem internet
                return 2;
        }
 
        printf("Connection to...");
 
        for(p = res;p != NULL; p = p->ai_next) {
                void *addr;
                char *ipver;
 
                // get the pointer to the address itself,
                // different fields in IPv4 and IPv6:
                if (p->ai_family == AF_INET) { // IPv4
                        struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
                        addr = &(ipv4->sin_addr);
                        ipver = "IPv4";
                } else { // IPv6
                        struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
                        addr = &(ipv6->sin6_addr);
                        ipver = "IPv6";
                }
 
                // convert the IP to a string and print it:
                inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
                printf("  %s: %s\n", ipver, ipstr);
		if(strcmp(ipstr,"200.17.33.160") == 0) {
			logNetwork(1);
			return 1;
		} else {
			logNetwork(0);
		}
        }
 
        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
 
        if(connect(sockfd, res->ai_addr, res->ai_addrlen) == -1)
        {
                printf("Connection error\n");
                //perror(argv[0]);
                return errno;
        }
        else
                printf("Connection successful\n");
 
        close(sockfd);
 
        freeaddrinfo(res); // free the linked list
 
        return 0;
}


#endif // FOO_H_
