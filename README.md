# README #

An easy lib for sniffing network in C.

### How do I get set up? ###

* Include "sniffex.h"
* Compile with gcc with libs -lpcap -pthread
* Run with root access level

### Documentation ###

There are 3 parameters

* Device name is the name of the device you want to sniff
* Number of the packets is the amout of packets you want to capture. Use -1 for infinite looping
* Expression is the filter you want to catch

### Examples ###

Use me_sniffa(char *device_name, int number_of_packets, char *expression)

* me_sniffa(NULL, -1, "ip")
* me_sniffa("wlan0", 100, "tcp")
* me_sniffa("eth0", 500, "tcp port 80")

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact